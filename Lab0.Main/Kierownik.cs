﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Kierownik : Pracownik
    {
        public override int Pracuj()
        {
            Console.WriteLine("Kierownik rozpoczyna pracę");
            base.Pracuj();
            return 1;
        }
    }
}
