﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Pracownik);
        public static Type B = typeof(Kierownik);
        public static Type C = typeof(Manager);

        public static string commonMethodName = "Pracuj";
    }
}
