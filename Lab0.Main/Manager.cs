﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Manager : Pracownik
    {
        public override int Pracuj()
        {
            Console.WriteLine("Manager rozpoczyna pracę");
            base.Pracuj();
            return 2;
        }
    }
}
