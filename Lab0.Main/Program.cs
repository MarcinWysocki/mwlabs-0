﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Pracownik> lista = new List<Pracownik>();
            lista.Add(new Kierownik());
            lista.Add(new Manager());
            lista.Add(new Kierownik());
            lista.Add(new Manager());
            lista.Add(new Manager());
            foreach (Pracownik pracownik in lista)
            {
                pracownik.Pracuj();
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
